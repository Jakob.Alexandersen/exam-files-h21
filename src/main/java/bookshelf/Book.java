package bookshelf;

/**
 * This class is a simple representation of a book
 * @author Sondre Bolland og Martin Vatshelle
 */
public class Book {
	
	private String title;
	private String author;
	
	/**
	 * Constructs a Book with given title and author
	 * @param title
	 * @param author
	 */
	public Book(String title, String author) {
		this.title = title;
		this.author = author;
	}
	
	@Override
	public boolean equals(Object object) {
		if (object == null)
			return false;
		if (this == object)
			return true;
		if (!(object instanceof Book))
			return false;
		
		Book otherBook = (Book) object;
		if (title.equals(otherBook.title))
			if (author.equals(otherBook.author))
				return true;
		return false;
	}
}
